# 과학관 관람객용 iOS 앱

- Android 앱 링크: <https://github.com/Itaz-DEV/Itaz-RnD2-science-mobile>

- iOS 앱 환경설정 및 빌드는 macOS 기준

## 환경설정

1. Flutter 설치
>   - Flutter version: 1.20.0
>   - Framework revision 840c9205b3 (2020-08-04)
>   - Engine revision c8e3b94853
>   - Dart version 2.9.0

2. 환경변수추가
>   - ~/.bashrc (bash) 또는 ~/.zshrc (Z shell)에 PATH 추가
>   <pre><code>export PATH="$PATH:`pwd`/flutter/bin"</code></pre>

3. Flutter doctor 실행
>   - 출력된 내용을 꼼꼼히 보고 설치해야 할 다른 소프트웨어가 있는지 또는 수행해야 할 추가 작업이 있는지 확인
>   <pre><code>$ flutter doctor</code></pre>

## 빌드

1. 소스내려받기
>   <pre><code>$ git clone https://gitlab.com/jinoo103/itaz-rnd2-science-ios.git</code></pre>

2. 패키지 설정
>   <pre><code>$ cd Itaz-RnD2-science-ios</code></pre>
>   <pre><code>flutter pub get</code></pre>

3. Xcode 실행
>   <pre><code>$ cd Itaz-RnD2-science-ios</code></pre>
>   <pre><code>$ open ios/Runner.xcworkspace</code></pre>

4. Xcode Project 설정
>   - Xcode Project 클릭
>       + General > Identity > 'Version' 설정
>       + Signing & Capabilities > Signing > 'Team' 및 'Bundle Identifier' 설정

5. Info.plist 설정추가
>   - NSBluetoothAlwaysUsageDescription: Our app uses bluetooth to scan exhibit 
>   - NSLocationWhenInUseUsageDescription: Your location is required for scan exhibit
>   - NSCameraUsageDescription: Our app requires access to your phone's camera

6. Debug
>   - Xcode > Product > Run

7. Build
>   <pre><code>$ cd Itaz-RnD2-science-ios</code></pre>
>   <pre><code>$ flutter run</code></pre>
>   <pre><code>$ flutter run --release</code></pre>

8. Distributing
>   - Distributing Your App for Beta Testing and Releases
>   <https://developer.apple.com/documentation/xcode/distributing-your-app-for-beta-testing-and-releases>
>   - Preparing Your App for Distribution
>   <https://developer.apple.com/documentation/xcode/preparing-your-app-for-distribution>
