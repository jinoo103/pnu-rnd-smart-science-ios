import 'package:flutter/material.dart';

class BeaconFootPrint with ChangeNotifier {
  List<int> footPrint = [
    0,
  ];
  int index = 0;
  BeaconFootPrint();
  getBeaconFootPrint() => footPrint;
  getFootPrintIndex() => index;

  addFootPrint(int newStep) {
    footPrint.add(newStep);
    notifyListeners();
  }

  increaseIndex() {
    index++;
  }
}
