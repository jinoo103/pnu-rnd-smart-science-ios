import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_beacon/flutter_beacon.dart';
import '../graphQLBloc/GraphQLService.dart';

class BeaconStream with ChangeNotifier {
  final gql = GraphQLService();

  StreamSubscription<BluetoothState> _streamBluetooth;
  StreamSubscription<RangingResult> _streamRanging;
  final _regionBeacons = <Region, List<Beacon>>{};
  final _beacons = <Beacon>[];
  bool authorizationStatusOk = false;
  bool locationServiceEnabled = false;

  bool bluetoothEnabled = false;

  final _beaconStateStreamController = StreamController<BluetoothState>();
  StreamSink<BluetoothState> get stateSink => _beaconStateStreamController.sink;
  Stream<BluetoothState> get stateStream => _beaconStateStreamController.stream;

  final _beaconStreamController = StreamController<Beacon>.broadcast();
  Stream<Beacon> get beaconStream => _beaconStreamController.stream;
  StreamSink<Beacon> get beaconSink => _beaconStreamController.sink;

  final _macStream = StreamController<String>.broadcast();
  Stream<String> get macStream => _macStream.stream;
  StreamSink<String> get macSink => _macStream.sink;

  final _majStream = StreamController<int>.broadcast();
  Stream<int> get majStream => _majStream.stream;
  StreamSink<int> get majSink => _majStream.sink;

  BeaconStream() {
    _streamBluetooth = flutterBeacon
        .bluetoothStateChanged()
        .listen((BluetoothState state) async {
      if (!authorizationStatusOk) await flutterBeacon.requestAuthorization;
      if (!bluetoothEnabled) await flutterBeacon.bluetoothState;
      //print('BluetoothState = $state');
      stateSink.add(state);
      switch (state) {
        case BluetoothState.stateOn:
          initScanBeacon();
          break;
        case BluetoothState.stateOff:
          await pauseScanBeacon();
          await checkAllRequirements();
          break;
      }
    });
  }
  checkAllRequirements() async {
    final bluetoothState = await flutterBeacon.bluetoothState;
    final bluetoothEnabled = bluetoothState == BluetoothState.stateOn;
    final authorizationStatus = await flutterBeacon.authorizationStatus;
    final authorizationStatusOk =
        authorizationStatus == AuthorizationStatus.allowed ||
            authorizationStatus == AuthorizationStatus.always;
    final locationServiceEnabled =
        await flutterBeacon.checkLocationServicesIfEnabled;
    this.authorizationStatusOk = authorizationStatusOk;
    this.locationServiceEnabled = locationServiceEnabled;
    this.bluetoothEnabled = bluetoothEnabled;
  }

  initScanBeacon() async {
    await flutterBeacon.initializeScanning;
    await checkAllRequirements();
    if (!authorizationStatusOk ||
        !locationServiceEnabled ||
        !bluetoothEnabled) {
      // print('RETURNED, authorizationStatusOk=$authorizationStatusOk, '
      //     'locationServiceEnabled=$locationServiceEnabled, '
      //     'bluetoothEnabled=$bluetoothEnabled');
      return;
    }
    final regions = <Region>[];
    if (Platform.isIOS) {
      // iOS platform, at least set identifier and proximityUUID for region scanning
      regions.add(Region(
          identifier: 'BlunoBeetle',
          proximityUUID: '0000dfb0-0000-1000-8000-00805f9b34fb'));
    } else {
      // android platform, it can ranging out of beacon that filter all of Proximity UUID
      regions.add(Region(identifier: 'com.beacon'));
    }

    if (_streamRanging != null) {
      if (_streamRanging.isPaused) {
        _streamRanging.resume();
        return;
      }
    }

    _streamRanging =
        flutterBeacon.ranging(regions).listen((RangingResult result) async {
      //print(result);
      if (result != null) {
        _regionBeacons[result.region] = result.beacons;
        _beacons.clear();
        _regionBeacons.values.forEach((list) {
          _beacons.addAll(list);
        });
        _beacons.sort(_compareParameters);
        //print(_beacons);

        // 2021.09.23 로직개선
        // _beacons.forEach((element) async {
        //   final check = await checkMac(element.macAddress);
        //   if (check) {
        //     if (element.accuracy < 7) {
        //       //얼마나 가까이 와야 하는지를 조절
        //       //accuracy가 낮을 수록 가까이 있다는 말
        //       beaconSink.add(element);
        //       macSink.add(element.macAddress);
        //       majSink.add(element.major);
        //       //accSink.add(element.accuracy);
        //     }
        //   }
        // });
        var temp = new Beacon(accuracy: 100.0);
        _beacons.forEach((element) {
            if (element.accuracy < 12) {
              if(temp.accuracy > element.accuracy){
                temp = element;
              }
            }
        });
        final check = await checkMac(temp.macAddress);
        if (check) {
          //얼마나 가까이 와야 하는지를 조절
          //accuracy가 낮을 수록 가까이 있다는 말
          beaconSink.add(temp);
          macSink.add(temp.macAddress);
          majSink.add(temp.major);
          //accSink.add(element.accuracy);
        }

      }
    });
  }

  checkMac(mac) async {
    String query = """
    query{
      macCheck(mac:"$mac")
    }
    """;
    final result = await gql.performQuery(query);
    // print(result.data["macCheck"]);
    return result.data["macCheck"];
  }

  pauseScanBeacon() async {
    _streamRanging?.pause();
    if (_beacons.isNotEmpty) {
      _beacons.clear();
    }
  }

  int _compareParameters(Beacon a, Beacon b) {
    int compare = a.proximityUUID.compareTo(b.proximityUUID);

    if (compare == 0) {
      compare = a.major.compareTo(b.major);
    }

    if (compare == 0) {
      compare = a.minor.compareTo(b.minor);
    }

    return compare;
  }

  dispose() {
    _beaconStreamController?.close();
    _beaconStateStreamController?.close();
    _macStream?.close();
    _majStream.close();
    _streamRanging?.cancel();
    _streamBluetooth?.cancel();
    flutterBeacon.close;
  }
}
