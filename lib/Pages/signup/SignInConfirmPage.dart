import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../graphQLBloc/GraphQLBloc.dart';
import '../../graphQLBloc/GraphQLStates.dart';
import 'package:hexcolor/hexcolor.dart';

class SignInConfirmPage extends StatefulWidget {
  SignInConfirmPage({Key key}) : super(key: key);

  @override
  _SignInConfirmPageState createState() => _SignInConfirmPageState();
}

class _SignInConfirmPageState extends State<SignInConfirmPage> {
  Map<String, dynamic> data;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: SafeArea(
        top: true,
        child: Scaffold(
            backgroundColor: HexColor("#a0b2de"),
            body: BlocBuilder<GraphQLBloc, GraphQLStates>(
              builder: (BuildContext context, GraphQLStates state) {
                if (state is Loading) {
                  return LinearProgressIndicator();
                } else if (state is LoadDataFail) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: Text("실패!"),
                      ),
                      RaisedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/signup');
                        },
                        child: Text("다시 시도"),
                      ),
                    ],
                  );
                } else if (state is LoadDataSuccess) {
                  data = (state).data['createUser'];
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 150,
                          child: Image(
                            image: AssetImage("images/logo.png"),
                          ),
                        ),
                        Container(
                          width: 350,
                          padding: EdgeInsets.all(12),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey[600],
                                  offset: const Offset(2.0, 3.0),
                                  blurRadius: 5.0,
                                  spreadRadius: 2.0,
                                ),
                              ],
                            ),
                            alignment: Alignment.center,
                            width: 350,
                            height: 100,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Name",
                                        style: TextStyle(fontSize: 10),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(data['name']),
                                    ],
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Gender",
                                        style: TextStyle(fontSize: 10),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(data['gender']),
                                    ],
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Age",
                                        style: TextStyle(fontSize: 10),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(data['age'].toString()),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        // Center(
                        //   child: Text(data['name'] +
                        //       " / " +
                        //       data['gender'] +
                        //       ' / ' +
                        //       data['age'].toString()),
                        // ),
                        Container(
                          width: 350,
                          padding: EdgeInsets.all(12),
                          child: RaisedButton(
                            color: Theme.of(context).primaryColor,
                            textColor: Colors.white,
                            onPressed: () {
                              Navigator.pushNamed(context, '/login');
                            },
                            child: Text("L O G I N"),
                          ),
                        ),
                      ],
                    ),
                  );
                } else
                  return Center(child: LinearProgressIndicator());
              },
            )),
      ),
    );
  }
}
