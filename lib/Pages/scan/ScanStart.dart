import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';
import '../../Pages/scan/mutationQueries.dart';
import '../../Pages/single/SinglePage.dart';
import '../../beacon/BeaconStream.dart';
import '../../beaconFootPrint/BeaconFootPrint.dart';
import '../../graphQLBloc/GraphQLBloc.dart';
import '../../graphQLBloc/GraphQLEvents.dart';
import '../../graphQLBloc/GraphQLStates.dart';
import '../../graphQLBloc/GraphQLService.dart';
import '../../timer/TimerBloc.dart';
import '../../timer/Timer.dart';
import '../../user/User.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'ProfilePage.dart';
import 'SearchPage.dart';
import 'package:permission_handler/permission_handler.dart';

class ScanStart extends StatefulWidget {
  ScanStart({Key key}) : super(key: key);

  @override
  _ScanStartState createState() => _ScanStartState();
}

class _ScanStartState extends State<ScanStart> with WidgetsBindingObserver {
  String query = "";
  String timeQuery = "";
  bool trigger = true;
  bool firstTime = true;
  int time = 0;
  int currzoneId = 0;
  int prevzoneId = 0;
  int userId;
  String userName;
  var mapURL;
  String footP = "";
  final gql = GraphQLService();
  List zstQueries = [];
  String qroutput = '';
  int currentCenterId = 0;
  int areaId = 0;
  String zoneName = "";
  bool _bluetooth = false;

  Future<QueryResult> _saveFootPrint() async {
    if (footP != "") {
      String footP2 = footP.substring(1, footP.length - 1);
      String query =
          'mutation{ createBeaconStack(userId:$userId, footPrint:"$footP2")}';
      return await gql.performQuery(query);
    } else {
      return null;
    }
  }

  Future<bool> _saveZoneSpentTimes() async {
    int lang = zstQueries.length;
    print("inside of SZST: $lang");
    for (int i = 0; i < lang; i++) {
      if (i == 0 || i == 1) {
        print("skip");
      } else {
        String query2 = zstQueries[i];
        // print(query2);
        await gql
            .performQuery(query2)
            ?.then((value) => print(value.toString()));
      }
    }
    await gql.performQuery(timeQuery)?.then((value) => print(value.toString()));
    return true;
  }

  Future<bool> _onWillPop() async {
    return (await showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Text("앱을 종료하시겠습니까?"),
                content: Text("스캔을 멈추면 앱이 종료됩니다."),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: Text('No'),
                  ),
                  FlatButton(
                    onPressed: () {
                      _saveFootPrint().then((value) {
                        print("save foot Print : $value");
                        _saveZoneSpentTimes().then((value2) {
                          print("save zone Spent Times : $value2");
                        }).then((value) => exit(0));
                      });
                    },
                    child: Text('Yes'),
                  ),
                ],
              );
            })) ??
        false;
  }

  Future<Widget> _scan() async {
    String barcode = await scanner.scan();
    setState(() => qroutput = barcode);
    String query =
        'query{getOneItem(itemName:"$barcode"){id, name, imageURL,desc}}';
    var result = await gql.performQuery(query);
    if (result.data != null) {
      var item = result.data['getOneItem'];
      return Navigator.push(context, MaterialPageRoute(builder: (context) {
        return SinglePage(item);
      }));
    } else {
      return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("등록 되지 않은 QR입니다."),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text('돌아가기'),
              ),
            ],
          );
        },
      );
    }
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    getPermi();
  }

  Future<void> getPermi() async {
    var status = await Permission.camera.status;
    if (status.isUndetermined) {
      await [
        Permission.camera,
        Permission.location,
      ].request();
    }
  }

  //홈버튼 눌렀을 때
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.paused) {
      await _saveZoneSpentTimes();
      await _saveFootPrint();
    }
    if (state == AppLifecycleState.resumed) {
      //print("--------------------------------------resumed");
    }
  }

  @override
  Widget build(BuildContext context) {
    final beacon = Provider.of<BeaconStream>(context);
    final user = Provider.of<User>(context);
    final footPrint = Provider.of<BeaconFootPrint>(context);
    final mutationQueries = Provider.of<MutationQueries>(context);
    List data = [];

    if (user.getAuth()) {
      setState(() {
        userId = user.getId();
        userName = user.getName();
      });
      return WillPopScope(
        onWillPop: _onWillPop,
        child: SafeArea(
          top: true,
          child: Scaffold(
            floatingActionButton: FloatingActionButton.extended(
              onPressed: () {
                return _scan();
              },
              label: Image(
                image: AssetImage("images/grtiny.png"),
              ),
            ),
            body: Center(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      width: 350,
                      padding: EdgeInsets.all(12),
                      margin: EdgeInsets.all(12),
                      height: 130,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("$userName" + " 님,",
                                    style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold,
                                    )),
                                Text("반갑습니다.",
                                    style: TextStyle(
                                      fontSize: 20,
                                    )),
                              ],
                            ),
                            Container(
                              child: Column(
                                children: [
                                  Container(
                                    width: 45,
                                    child: RaisedButton(
                                      color: Colors.white,
                                      child: Image(
                                        image: AssetImage("images/search.png"),
                                      ),
                                      onPressed: () {
                                        Navigator.push(context,
                                            MaterialPageRoute(
                                                builder: (context) {
                                          return SearchPage(currentCenterId);
                                        }));
                                      },
                                    ),
                                  ),
                                  Container(
                                    width: 45,
                                    child: RaisedButton(
                                      color: Colors.white,
                                      child: Image(
                                        image:
                                            AssetImage("images/favoriteon.png"),
                                      ),
                                      onPressed: () {
                                        Navigator.push(context,
                                            MaterialPageRoute(
                                                builder: (context) {
                                          return ProfilePage(userId);
                                        }));
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            )
                          ]),
                    ),
                    //중첩 스트림 빌더로 스캔 데이터로 패치하여 화면 전환하는 부분
                    Container(
                      height: 400,
                      padding: EdgeInsets.all(15),
                      child: StreamBuilder(
                        stream: beacon.macStream.asBroadcastStream(),
                        builder: (context, snapshot2) {
                          if (snapshot2 != null &&
                              snapshot2.data != null &&
                              !snapshot2.hasError) {
                            SchedulerBinding.instance.addPostFrameCallback((_) {
                              setState(() {
                                query =
                                    'query{getOneZone(macAddr:"${snapshot2.data}"){id,name, items{id,name,desc,imageURL},centerItSelf{id} area{id} mapURL}}';
                              });
                            });
                          }
                          return StreamBuilder(
                            stream: beacon.majStream.asBroadcastStream(),
                            builder: (context, snapshot) {
                              if (snapshot != null &&
                                  snapshot.data != null &&
                                  !snapshot.hasError) {
                                if (footPrint.footPrint[footPrint.index] !=
                                    snapshot.data) {
                                  SchedulerBinding.instance
                                      .addPostFrameCallback((_) {
                                    footPrint.addFootPrint(snapshot.data);
                                  });
                                  SchedulerBinding.instance
                                      .addPostFrameCallback((_) {
                                    setState(() {
                                      footP = footPrint.footPrint.toString();
                                      trigger = true;
                                    });
                                  });
                                  footPrint.increaseIndex();
                                  return LinearProgressIndicator();
                                }
                                if (query != "" || query != null) {
                                  return BlocProvider<GraphQLBloc>(
                                    create: (BuildContext context) =>
                                        GraphQLBloc()..add(FetchGQLData(query)),
                                    child: Container(
                                      padding: EdgeInsets.all(15),
                                      width: double.infinity,
                                      height: 600,
                                      child: BlocBuilder<GraphQLBloc,
                                          GraphQLStates>(
                                        builder: (BuildContext context,
                                            GraphQLStates state) {
                                          if (state is Loading) {
                                            return Container(
                                              alignment: Alignment.topCenter,
                                              child: LinearProgressIndicator(),
                                            );
                                          } else if (state is LoadDataFail) {
                                            return Center(
                                              child: Text("등록되지 않은 비콘"),
                                            );
                                          } else {
                                            if (state is LoadDataSuccess) {
                                              data = state.data['getOneZone']
                                                  ['items'];
                                              SchedulerBinding.instance
                                                  .addPostFrameCallback((_) {
                                                setState(() {
                                                  zoneName =
                                                      state.data['getOneZone']
                                                          ['name'];
                                                  currzoneId = state
                                                      .data['getOneZone']['id'];
                                                  currentCenterId = state
                                                          .data['getOneZone']
                                                      ['centerItSelf']['id'];
                                                  areaId =
                                                      state.data['getOneZone']
                                                          ['area']['id'];
                                                  mapURL =
                                                      state.data['getOneZone']
                                                          ['mapURL'];
                                                });
                                              });
                                              return Container(
                                                child: ListView.builder(
                                                  scrollDirection:
                                                      Axis.horizontal,
                                                  itemCount: data.length,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    var item = data[index];
                                                    return GestureDetector(
                                                      onTap: () {
                                                        Navigator.push(context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) {
                                                          return SinglePage(
                                                              item);
                                                        }));
                                                      },
                                                      child: Card(
                                                        elevation: 1.0,
                                                        child:
                                                            SingleChildScrollView(
                                                          child: Container(
                                                            width: 200,
                                                            height: 1000,
                                                            margin:
                                                                EdgeInsets.all(
                                                                    10),
                                                            child: Column(
                                                              children: [
                                                                Container(
                                                                  margin:
                                                                      EdgeInsets
                                                                          .all(
                                                                              12),
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: Colors
                                                                        .white,
                                                                    borderRadius:
                                                                        BorderRadius.all(
                                                                            Radius.circular(10)),
                                                                    boxShadow: [
                                                                      BoxShadow(
                                                                        color: Colors
                                                                            .grey[200],
                                                                        offset: const Offset(
                                                                            2.0,
                                                                            3.0),
                                                                        blurRadius:
                                                                            5.0,
                                                                        spreadRadius:
                                                                            2.0,
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  child: Image(
                                                                    image: NetworkImage(
                                                                        item[
                                                                            'imageURL']),
                                                                    loadingBuilder: (BuildContext
                                                                            context,
                                                                        Widget
                                                                            child,
                                                                        ImageChunkEvent
                                                                            loadingProgress) {
                                                                      if (loadingProgress ==
                                                                          null)
                                                                        return child;
                                                                      return Center(
                                                                        child:
                                                                            CircularProgressIndicator(
                                                                          value: loadingProgress.expectedTotalBytes != null
                                                                              ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
                                                                              : null,
                                                                        ),
                                                                      );
                                                                    },
                                                                  ),
                                                                ),
                                                                Text(
                                                                  item['name'],
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        13,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                                SizedBox(
                                                                  height: 10,
                                                                ),
                                                                Text(
                                                                  item['desc'],
                                                                  style:
                                                                      TextStyle(
                                                                          height:
                                                                              2),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                ),
                                              );
                                            } else
                                              return CircularProgressIndicator();
                                          }
                                        },
                                      ),
                                    ),
                                  );
                                } else {
                                  return Text("Loading...");
                                }
                              } else {
                                return Column(children: [
                                  Text(
                                      "블루투스 및 위치 기능이 꺼져있다면 \n 활성화 후 앱을 다시 실행해주세요. \n",
                                      textAlign: TextAlign.center,
                                      style:
                                          TextStyle(color: Colors.grey[500])),
                                  Text("\n 주변 전시물을 찾는 중...")
                                ]);
                              }
                            },
                          );
                        },
                      ),
                    ),
                    //지도 정보
                    Container(
                      alignment: Alignment.center,
                      width: 350,
                      padding: EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25),
                            topRight: Radius.circular(25)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[200],
                            offset: const Offset(2.0, 3.0),
                            blurRadius: 5.0,
                            spreadRadius: 2.0,
                          ),
                        ],
                      ),
                      child: Text(
                        "$userName 님의 현재 위치 : "
                        "$zoneName",
                        style: TextStyle(fontSize: 13),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(25),
                            bottomRight: Radius.circular(25)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[200],
                            offset: const Offset(2.0, 3.0),
                            blurRadius: 5.0,
                            spreadRadius: 2.0,
                          ),
                        ],
                      ),
                      width: 350,
                      padding: EdgeInsets.all(12),
                      child: Image(
                        image: NetworkImage("$mapURL"),
                        loadingBuilder: (BuildContext context, Widget child,
                            ImageChunkEvent loadingProgress) {
                          if (loadingProgress == null) return child;
                          return Center(
                            child: CircularProgressIndicator(
                              value: loadingProgress.expectedTotalBytes != null
                                  ? loadingProgress.cumulativeBytesLoaded /
                                      loadingProgress.expectedTotalBytes
                                  : null,
                            ),
                          );
                        },
                      ),
                    ),
                    //타이머 부분
                    BlocProvider(
                      create: (_) => TimerBloc(Timer())..add(TimerStarted()),
                      child: Container(
                        padding: EdgeInsets.all(15),
                        child: BlocBuilder<TimerBloc, TimerStates>(
                          builder: (context, state) {
                            if (state is TimerTickSuccess) {
                              if (trigger == true) {
                                if (firstTime == true) {
                                  SchedulerBinding.instance
                                      .addPostFrameCallback((_) {
                                    setState(() {
                                      trigger = false;
                                      firstTime = false;
                                    });
                                  });
                                } else {
                                  BlocProvider.of<TimerBloc>(context)
                                      .add(TimerStopped(0));
                                  BlocProvider.of<TimerBloc>(context)
                                      .add(TimerStarted());
                                  SchedulerBinding.instance
                                      .addPostFrameCallback((_) {
                                    mutationQueries.addqueries(timeQuery);
                                    setState(() {
                                      zstQueries = mutationQueries.getQueries();
                                      trigger = false;
                                    });
                                  });
                                }
                              }
                              SchedulerBinding.instance
                                  .addPostFrameCallback((_) {
                                setState(() {
                                  timeQuery =
                                      'mutation{createSpentTime(areaId:$areaId centerId:$currentCenterId zoneId:$currzoneId, userId:$userId, time:$time){time, id}}';
                                  time = state.count;
                                });
                              });
                              return Text("");
                            }
                            return (Text(""));
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    } else {
      return Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Text("로그인 하시오"),
              ),
              Container(
                child: RaisedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/login');
                  },
                  child: Text("login"),
                ),
              ),
            ],
          ),
        ),
      );
    }
  }
}
