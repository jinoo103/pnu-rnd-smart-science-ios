import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import '../../Pages/single/SinglePage.dart';
import '../../graphQLBloc/GraphQLService.dart';

class ProfilePage extends StatefulWidget {
  final int userId;

  ProfilePage(this.userId);

  _ProfilePageState createState() => _ProfilePageState(userId);
}

class _ProfilePageState extends State<ProfilePage> {
  int userId;
  String userName;
  String userGender;
  int userAge;
  String itemName;
  String itemURL;
  String itemDesc;

  _ProfilePageState(this.userId);

  final gql = GraphQLService();

  Future<Map> _user() async {
    Map<String, dynamic> result;
    String userQuery = "query{user(id:$userId){name, gender, age}}";
    var user = await gql.performQuery(userQuery);
    result = user.data;
    return result;
  }

  Future<List> _favorite() async {
    List result = [];
    String favoriteQuery =
        "query{getUserRelatedFavorites(userId:$userId){item{id, name, imageURL, desc}}}";
    var favorite = await gql.performQuery(favoriteQuery);
    if (favorite.data != null) {
      result = favorite.data['getUserRelatedFavorites'];
      return result;
    } else
      return null;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FutureBuilder<Map>(
                  future: _user(),
                  // Text(snapshot.data[0]['item']['id'].toString()
                  builder: (context, snapshot) {
                    if (snapshot.hasData && snapshot.data != null) {
                      SchedulerBinding.instance
                          .addPostFrameCallback((timeStamp) {
                        setState(() {
                          userName = snapshot.data['user']['name'];
                        });
                      });
                      return Column(
                        children: [
                          SizedBox(
                            height: 50,
                          ),
                          Container(
                            width: 350,
                            padding: EdgeInsets.all(12),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey[200],
                                    offset: const Offset(2.0, 3.0),
                                    blurRadius: 5.0,
                                    spreadRadius: 2.0,
                                  ),
                                ],
                              ),
                              alignment: Alignment.center,
                              width: 350,
                              height: 100,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Container(
                                    alignment: Alignment.center,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Name",
                                          style: TextStyle(fontSize: 10),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                            "${snapshot.data['user']['name']}"),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Gender",
                                          style: TextStyle(fontSize: 10),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                            "${snapshot.data['user']['gender']}"),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Age",
                                          style: TextStyle(fontSize: 10),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text("${snapshot.data['user']['age']}"),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
                FutureBuilder<List>(
                  future: _favorite(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Column(
                        children: [
                          Container(
                            margin: EdgeInsets.all(40),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey[200],
                                  offset: const Offset(2.0, 3.0),
                                  blurRadius: 5.0,
                                  spreadRadius: 2.0,
                                ),
                              ],
                            ),
                            width: 400,
                            height: 500,
                            child: ListView.builder(
                              itemCount: snapshot.data.length,
                              itemBuilder: (context, index) {
                                var item = snapshot.data[index];
                                return Container(
                                  padding: EdgeInsets.all(5),
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.push(context,
                                          MaterialPageRoute(builder: (context) {
                                        return SinglePage(item['item']);
                                      }));
                                    },
                                    child: ListTile(
                                      // hoverColor: Colors.blueGrey,
                                      leading: CircleAvatar(
                                        backgroundImage: NetworkImage(
                                            "${item['item']['imageURL']}"),
                                      ),

                                      title: Text("${item['item']['name']}"),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      );
                    }
                    return Center(
                        child: Container(
                      child: Text("즐겨찾기 전시물이 없습니다."),
                      padding: EdgeInsets.all(20),
                    ));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
