import 'package:flutter/material.dart';

class MutationQueries with ChangeNotifier {
  List<String> queries = [
    "mutation{dummy()}",
  ];
  int index = 0;
  MutationQueries();
  getQueries() => queries;
  getQueriesIndex() => index;

  addqueries(String newStep) {
    queries.add(newStep);
    notifyListeners();
  }

  increaseIndex() {
    index++;
  }
}
