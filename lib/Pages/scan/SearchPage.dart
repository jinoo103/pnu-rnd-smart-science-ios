import 'package:flutter/material.dart';
// import 'package:flutter/scheduler.dart';
import '../../Pages/single/SinglePage.dart';
import '../../graphQLBloc/GraphQLService.dart';

class SearchPage extends StatefulWidget {
  SearchPage(this.currentCenterId);
  final currentCenterId;
  _SearchPageState createState() => _SearchPageState(currentCenterId);
}

class _SearchPageState extends State<SearchPage> {
  final gql = GraphQLService();
  String searchword = "";
  final TextEditingController searchwordCon = TextEditingController();
  int currentCenterId;
  _SearchPageState(this.currentCenterId);

  void _submittedSearchword(String text) {
    setState(() {
      searchword = text;
    });
  }

  Future<List> _allitems() async {
    List result = [];
    String itemsQuery =
        "query{getAllItems(centerId:$currentCenterId){id,name,desc,imageURL}}";
    var favorite = await gql.performQuery(itemsQuery);
    if (favorite.data != null) {
      result = favorite.data['getAllItems'];
      return result;
    } else {
      return result;
    }
  }

  @override
  void dispose() {
    searchwordCon.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        child: Scaffold(
            body: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                FutureBuilder<List>(
                  future: _allitems(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData && snapshot.data != null) {
                      return Column(
                        children: [
                          Container(
                            width: 350,
                            padding: EdgeInsets.all(12),
                            child: TextField(
                              controller: searchwordCon,
                              decoration: InputDecoration(labelText: "search"),
                              onSubmitted: _submittedSearchword,
                              onChanged: (text) {
                                setState(() {
                                  searchword = text;
                                });
                              },
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(40),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey[200],
                                  offset: const Offset(2.0, 3.0),
                                  blurRadius: 5.0,
                                  spreadRadius: 2.0,
                                ),
                              ],
                            ),
                            width: 350,
                            height: 600,
                            child: ListView.builder(
                              itemCount: snapshot.data.length,
                              itemBuilder: (context, index) {
                                var item = snapshot.data[index];
                                if (searchword == "") {
                                  return Container(
                                    padding: EdgeInsets.all(5),
                                    child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(context,
                                            MaterialPageRoute(
                                                builder: (context) {
                                          return SinglePage(item);
                                        }));
                                      },
                                      child: ListTile(
                                        // hoverColor: Colors.blueGrey,
                                        leading: CircleAvatar(
                                          backgroundImage: NetworkImage(
                                              "${item['imageURL']}"),
                                        ),
                                        title: Text("${item['name']}"),
                                      ),
                                    ),
                                  );
                                } else if (item['name'].contains(searchword)) {
                                  //검색 수정 필요한 경우
                                  return ListTile(
                                    leading: CircleAvatar(
                                      backgroundImage:
                                          NetworkImage("${item['imageURL']}"),
                                    ),
                                    title: Text("${item['name']}"),
                                  );
                                } else {
                                  return Container();
                                }
                              },
                            ),
                          )
                        ],
                      );
                    }
                    return Center(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 300,
                            ),
                            Container(
                              child: Text("Loading..."),
                            ),
                          ]),
                    );
                  },
                ),
              ],
            ),
          ),
        )));
  }
}
