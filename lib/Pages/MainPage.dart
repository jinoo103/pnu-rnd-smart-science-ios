import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../Pages/scan/mutationQueries.dart';
import '../beaconFootPrint/BeaconFootPrint.dart';
import 'login/LoginPage.dart';
import 'home/HomePage.dart';
import 'signup/SignUpPage.dart';
import 'scan/ScanStart.dart';
import '../user/User.dart';
import '../beacon/BeaconStream.dart';
import 'package:hexcolor/hexcolor.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<User>(
          create: (_) => User(),
        ),
        ChangeNotifierProvider<BeaconFootPrint>(
          create: (_) => BeaconFootPrint(),
        ),
        ChangeNotifierProvider<BeaconStream>(
          create: (_) => BeaconStream(),
        ),
        ChangeNotifierProvider<MutationQueries>(
          create: (_) => MutationQueries(),
        ),
      ],
      child: MaterialApp(
        theme: ThemeData(
          primaryColor: HexColor("#6b7cc2"),
          accentColor: HexColor("#6b7cc2"),
        ),
        initialRoute: '/login',
        routes: {
          '/': (context) => HomePage(),
          '/login': (context) => LoginPage(),
          '/signup': (context) => SignUpPage(),
          '/scan': (context) => ScanStart(),
        },
      ),
    );
  }
}
