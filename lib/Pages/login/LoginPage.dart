import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../graphQLBloc/GraphQLBloc.dart';
import '../../graphQLBloc/GraphQLEvents.dart';
import 'LoginConfirmPage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String username = "";
  String password = "";
  SharedPreferences localStorage;
  final TextEditingController _usernameCon = TextEditingController();
  final TextEditingController _passwordCon = TextEditingController();
  String query = "";
  bool checkCached;

  @override
  void dispose() {
    _usernameCon.dispose();
    _passwordCon.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    checkLocalStorage();
  }

  //로컬스토리지
  void checkLocalStorage() async {
    localStorage = await SharedPreferences.getInstance();
    checkCached = (localStorage.getBool('isCached') ?? true);
    //print(checkCached);
    if (checkCached == false)
      return;
    else {
      username = localStorage.getString('username');
      password = localStorage.getString('password');
      _usernameCon.text = username;
      _passwordCon.text = password;
      query = '''
      query{simpleAuth(_name:"$username", _password:"$password"){
        id, name, gender, age
      }}
      ''';
    }
  }

  void _submittedUsername(String text) {
    setState(() {
      username = text;
    });
  }

  void _submittedPassword(String text) {
    setState(() {
      password = text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Text(username),
                // Text(password),
                // Text(query),
                Container(
                  width: 150,
                  child: Image(
                    image: AssetImage("images/logo.png"),
                  ),
                ),
                Container(
                  width: 350,
                  padding: EdgeInsets.all(12),
                  child: TextField(
                    controller: _usernameCon,
                    decoration:
                        InputDecoration(labelText: "your name or nickname"),
                    onSubmitted: _submittedUsername,
                    onChanged: (text) {
                      setState(() {
                        username = text;
                        query = '''
                        query{simpleAuth(_name:"$username", _password:"$password"){
                          id, name, gender, age
                        }}
                        ''';
                      });
                    },
                  ),
                ),
                Container(
                  width: 350,
                  padding: EdgeInsets.all(12),
                  child: TextField(
                    controller: _passwordCon,
                    obscureText: true,
                    decoration: InputDecoration(labelText: "password"),
                    onSubmitted: _submittedPassword,
                    onChanged: (text) {
                      setState(() {
                        password = text;
                        query = '''
                        query{simpleAuth(_name:"$username", _password:"$password"){
                          name, gender, age, id
                        }}
                        ''';
                      });
                    },
                  ),
                ),
                Container(
                  width: 350,
                  padding: EdgeInsets.all(12),
                  child: RaisedButton(
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    onPressed: () async {
                      localStorage = await SharedPreferences.getInstance();
                      localStorage.setString('username', username);
                      localStorage.setString('password', password);
                      localStorage.setBool('isCached', true);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return MultiBlocProvider(
                          providers: [
                            BlocProvider<GraphQLBloc>(
                              create: (BuildContext context) =>
                                  GraphQLBloc()..add(FetchGQLData(query)),
                            ),
                          ],
                          child: LoginConfirmPage(),
                        );
                      }));
                    },
                    child: Text("login"),
                  ),
                ),
                Container(
                  width: 350,
                  padding: EdgeInsets.all(12),
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    onPressed: () {
                      Navigator.pushNamed(context, '/signup');
                    },
                    child: Text(" Sign Up !"),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
