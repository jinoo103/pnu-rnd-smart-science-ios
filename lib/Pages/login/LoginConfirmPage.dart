import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:provider/provider.dart';
import '../../user/User.dart';
import '../../graphQLBloc/GraphQLBloc.dart';
import '../../graphQLBloc/GraphQLStates.dart';

class LoginConfirmPage extends StatefulWidget {
  LoginConfirmPage({Key key}) : super(key: key);

  @override
  _LoginConfirmPageState createState() => _LoginConfirmPageState();
}

class _LoginConfirmPageState extends State<LoginConfirmPage> {
  Map<String, dynamic> result;
  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context);
    return WillPopScope(
      onWillPop: () async {
        return Navigator.pushNamed(context, '/');
      },
      child: Scaffold(
        backgroundColor: HexColor("#a0b2de"),
        body: BlocBuilder<GraphQLBloc, GraphQLStates>(
          builder: (BuildContext context, GraphQLStates state) {
            if (state is Loading) {
              return LinearProgressIndicator();
            } else if (state is LoadDataFail) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Text("등록되지 않은 계정입니다."),
                  ),
                  Container(
                    width: 350,
                    padding: EdgeInsets.all(12),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.pushNamed(context, '/login');
                      },
                      child: Text("다시 로그인해 주세요"),
                    ),
                  ),
                ],
              );
            } else if (state is LoadDataSuccess) {
              result = (state).data["simpleAuth"];
              if (result != null) {
                String n = result['name'];
                String g = result['gender'];
                int a = result['age'];
                int i = result['id'];
                user.setUser(n, g, a, i);
                user.changeAuthState(true);
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 150,
                        child: Image(
                          image: AssetImage("images/logo.png"),
                        ),
                      ),
                      Container(
                        width: 350,
                        padding: EdgeInsets.all(12),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[600],
                                offset: const Offset(2.0, 3.0),
                                blurRadius: 5.0,
                                spreadRadius: 2.0,
                              ),
                            ],
                          ),
                          alignment: Alignment.center,
                          width: 350,
                          height: 100,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "Name",
                                      style: TextStyle(fontSize: 10),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(user.getName()),
                                  ],
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "Gender",
                                      style: TextStyle(fontSize: 10),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(user.getGender()),
                                  ],
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "Age",
                                      style: TextStyle(fontSize: 10),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(user.getAge().toString()),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        width: 350,
                        padding: EdgeInsets.all(12),
                        child: RaisedButton(
                          color: Theme.of(context).primaryColor,
                          textColor: Colors.white,
                          onPressed: () {
                            Navigator.pushNamed(context, '/');
                          },
                          child: Text("TO HOME"),
                        ),
                      ),
                    ],
                  ),
                );
              } else {
                return Container(child: Text("로그인 실패!\n네트워크 불안정"));
              }
            } else
              return Center(
                child: CircularProgressIndicator(),
              );
          },
        ),
      ),
    );
  }
}
