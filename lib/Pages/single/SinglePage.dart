import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';
import '../../graphQLBloc/GraphQLService.dart';
import '../../timer/TimerBloc.dart';
import '../../timer/Timer.dart';
import '../../user/User.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class SinglePage extends StatefulWidget {
  final item;

  SinglePage(this.item);

  @override
  _SinglePageState createState() => _SinglePageState(item);
}

class _SinglePageState extends State<SinglePage> {
  _SinglePageState(this.item);
  var item;
  int userId;
  int itemId;
  final gql = GraphQLService();
  int time;
  var rating = 0.0;
  var ratingToSend = 0.0;

  Future<QueryResult> _addFavorite() async {
    String query = 'mutation{createFavorite(userId:$userId, itemId:$itemId)}';
    return await gql.performQuery(query);
  }

  Future<QueryResult> _addSpentTimeItem() async {
    String query2 =
        'mutation{createSpentTimeItem(userId:$userId, itemId:$itemId, time:$time){item{name}, time}}';
    return await gql.performQuery(query2);
  }

  Future<QueryResult> _addRating() async {
    String query =
        'mutation{createRating(userId:$userId, itemId:$itemId,rating:$ratingToSend)}';
    return await gql.performQuery(query);
  }

  _showMaterialDialog(data) {
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              title: Text("반영되었습니다."),
              content: Text(""),
              actions: <Widget>[
                FlatButton(
                  child: Text('Close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
  }

  Future<bool> _onWillback() async {
    return (await showDialog(
            context: context,
            builder: (_) {
              return AlertDialog(
                title: Text(""),
                content: Text("스캔 화면으로 넘어갑니다."),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: Text('No'),
                  ),
                  FlatButton(
                    onPressed: () {
                      _addSpentTimeItem().then((value) {
                        return Navigator.of(context).pop(true);
                      });
                    },
                    child: Text('Yes'),
                  ),
                ],
              );
            })) ??
        false;
  }

  Stream<int> timerInside() {
    return Stream.periodic(const Duration(seconds: 1), (x) => x);
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    setState(() {
      itemId = item['id'];
      userId = user.getId();
    });
    return SafeArea(
      top: true,
      child: WillPopScope(
        onWillPop: _onWillback,
        child: Scaffold(
          body: SingleChildScrollView(
            child: Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        Container(
                          margin: EdgeInsets.all(12),
                          width: 400,
                          height: 50,
                          padding: EdgeInsets.all(12),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                child: Text("즐겨찾기에 추가"),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Container(
                                width: 45,
                                height: 45,
                                alignment: Alignment.center,
                                child: RaisedButton(
                                  color: Colors.white,
                                  onPressed: () {
                                    _addFavorite().then((value) {
                                      _showMaterialDialog(value.data);
                                    });
                                  },
                                  child: Image(
                                    image: AssetImage("images/favoriteon.png"),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(12),
                          width: 350,
                          padding: EdgeInsets.all(12),
                          child: Text(item['name'],
                              style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                        Container(
                          margin: EdgeInsets.all(12),
                          width: 400,
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[200],
                                offset: const Offset(2.0, 3.0),
                                blurRadius: 1.0,
                                spreadRadius: 1.0,
                              ),
                            ],
                          ),
                          child: Image(
                            image: NetworkImage(item['imageURL']),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(12),
                          width: 400,
                          padding: EdgeInsets.all(12),
                          child: Text(
                            item['desc'],
                            style: TextStyle(
                              fontSize: 14,
                              height: 2,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(12),
                          width: 350,
                          padding: EdgeInsets.all(12),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                margin: EdgeInsets.all(12),
                                padding: EdgeInsets.all(10),
                                child: SmoothStarRating(
                                  rating: rating,
                                  isReadOnly: false,
                                  size: 40,
                                  filledIconData: Icons.star,
                                  halfFilledIconData: Icons.star_half,
                                  defaultIconData: Icons.star_border,
                                  starCount: 5,
                                  allowHalfRating: true,
                                  spacing: 2.0,
                                  color: Theme.of(context).primaryColor,
                                  borderColor: Theme.of(context).primaryColor,
                                  onRated: (value) {
                                    setState(() {
                                      ratingToSend = value;
                                    });
                                  },
                                ),
                              ),
                              Container(
                                width: 350,
                                child: RaisedButton(
                                  color: Theme.of(context).primaryColor,
                                  textColor: Colors.white,
                                  onPressed: () {
                                    _addRating().then((value) {
                                      _showMaterialDialog(value.data);
                                    });
                                  },
                                  child: Text("나의 별점 등록"),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    BlocProvider(
                      create: (_) => TimerBloc(Timer())..add(TimerStarted()),
                      child: Container(
                        padding: EdgeInsets.all(15),
                        child: BlocBuilder<TimerBloc, TimerStates>(
                          builder: (context, state) {
                            if (state is TimerTickSuccess) {
                              SchedulerBinding.instance
                                  .addPostFrameCallback((_) {
                                setState(() {
                                  time = state.count;
                                });
                              });
                              return Text("");
                            }
                            return (Text(""));
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
