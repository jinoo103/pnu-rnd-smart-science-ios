import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var rating = 0.0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
          body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 150,
              child: Image(
                image: AssetImage("images/logo.png"),
              ),
            ),
            Container(
              width: 350,
              padding: EdgeInsets.all(12),
              child: RaisedButton(
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
                onPressed: () {
                  Navigator.pushNamed(context, '/scan');
                },
                child: Text("SCAN START !"),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
