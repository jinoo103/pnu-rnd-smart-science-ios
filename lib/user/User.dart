import 'package:flutter/cupertino.dart';

class User with ChangeNotifier {
  User() {
    _name = "null";
    _gender = "null";
    _age = -1;
    _id = -1;
    _auth = false;
  }
  String _name;
  String _gender;
  int _age;
  int _id;
  bool _auth;
  String getName() => _name;
  String getGender() => _gender;
  int getAge() => _age;
  int getId() => _id;
  bool getAuth() => _auth;
  void setUser(String n, String g, int a, int i) {
    _name = n;
    _gender = g;
    _age = a;
    _id = i;
  }

  void changeAuthState(bool change) {
    _auth = change;
  }
}
